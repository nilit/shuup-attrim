import 'rxjs/Rx'
import 'reflect-metadata'
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic'

import {AppModule} from 'app/app.module'
import {enableProdMode} from '@angular/core'


enableProdMode()
// noinspection JSIgnoredPromiseFromCall
platformBrowserDynamic().bootstrapModule(AppModule)
