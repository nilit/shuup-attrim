import {Component} from '@angular/core'


@Component({
    selector: 'app',
    template: `<cls-form></cls-form>`,
})
export class AppComponent { }
