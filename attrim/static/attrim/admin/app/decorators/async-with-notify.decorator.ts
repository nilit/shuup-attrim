import {NotifyService} from 'app/services/notify.service'


/** It will break without the semicolons. */
export function asyncWithNotify(messages: {onSuccessMsg?: string, onErrorMsg: string}) {
    return (target: Object, propertyKey: string, descriptor: TypedPropertyDescriptor<any>) => {
        let originalMethod = descriptor.value
        descriptor.value = async function(...args: any[]): Promise<any> {
            let notifyService = (this as Notifiable).notifyService
            let result: any
            try {
                result = await originalMethod.apply(this, args);
                if (messages.onSuccessMsg !== undefined) {
                    notifyService.success(messages.onSuccessMsg);
                }
            } catch (error) {
                notifyService.error(messages.onErrorMsg, error);
            }
            return result
        }
        return descriptor
    }
}


export interface Notifiable {
    notifyService: NotifyService
}
