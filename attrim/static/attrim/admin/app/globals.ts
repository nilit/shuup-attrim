export type LangCode = string


export type PrimaryKey = number


export type Optional<T> = T | null


//noinspection JSUnusedGlobalSymbols
export interface Window {
    DJANGO: DjangoProvidedGlobals
}


export interface DjangoProvidedGlobals {
    langCodes: Array<LangCode>
    defaultLang: LangCode
    clsPrimaryKey?: Optional<number>
    isEditForm: boolean
}
