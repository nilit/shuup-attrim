import {FormsModule} from '@angular/forms'
import {async} from '@angular/core/testing'
import {By, BrowserModule} from '@angular/platform-browser'
import {TestBed} from '@angular/core/testing'
import {ComponentFixture} from '@angular/core/testing'
import {DebugElement} from '@angular/core'
import {ClsFormComponent} from 'app/cls-form/cls-form.comp'
import {OptionFormComponent} from 'app/cls-form/fields/option-form.comp'
import {TransStrComponent} from 'app/cls-form/fields/trans-str.comp'
import {NgModule} from '@angular/core'
import {Option} from 'app/models/option.model'
import {Cls} from 'app/models/cls.model'
import {Type} from 'app/type.enum'
import {ClsNetworkService} from 'app/services/network/cls.service'
import {IterateEnumPipe} from 'app/cls-form/iter-enum.pipe'
import {ClsJsonSerializerService} from 'app/services/network/serializers/cls-json.service'
import {ProductTypeNetworkService} from 'app/services/network/product-type.service'
import {ProductType} from 'app/models/product-type.model'
import {NotifyService} from 'app/services/notify.service'
import {NotificationsService} from 'angular2-notifications'
import {SlimLoadingBarModule} from 'ng2-slim-loading-bar'
import {OptionJsonSerializerService} from 'app/services/network/serializers/option-json.service'
import {OptionNetworkService} from 'app/services/network/option.service'
import {ProductTypeJsonSerializerService} from 'app/services/network/serializers/product-type-json.service'
import {HttpModule} from '@angular/http'
import {CookieService} from 'app/services/network/cookie.service'
import {CsrfService} from 'app/services/network/csrf.service'
import {SimpleNotificationsModule} from 'angular2-notifications'
import {PrimaryKey} from 'app/globals'
import {SuiModule} from 'ng2-semantic-ui'


describe('cls form tests', () => {
    let self: {
        fixture: ComponentFixture<ClsFormComponent>
        debugElem: DebugElement
        component: ClsFormComponent
    }

    beforeEach(async done => {
        TestBed.configureTestingModule({
            imports: [FakeModule],
        })
        await TestBed.compileComponents()
        done()
    })

    beforeEach(() => {
        initTestCase()
        resetTypeInput()
    })

    it('handles the name update', () => {
        let clsNameSection = self.debugElem.query(By.css('#cls-name'))
        let input = clsNameSection.query(By.css('input'))
        input.nativeElement.value = 'new value'
        input.nativeElement.dispatchEvent(new Event('input'))

        self.fixture.detectChanges()

        let updatedButtonClass = '.checkmark.box'
        let updatedButton = clsNameSection.query(By.css(updatedButtonClass))
        expect(updatedButton).not.toBeNull()
    })

    it('updates the option-form trans-str buttons status on an input change', () => {
        let addOptionButton = self.debugElem.query(By.css('#add-option'))
        clickOn(addOptionButton)
        clickOn(addOptionButton)

        let input = self.debugElem.query(By.css('trans-str input'))
        input.nativeElement.value = 'new value'
        input.nativeElement.dispatchEvent(new Event('input'))

        self.fixture.detectChanges()

        let updatedButtonClass = '.checkmark.box'
        let updatedButton = self.debugElem.query(By.css(updatedButtonClass))
        expect(updatedButton).not.toBeNull()
    })

    function initTestCase() {
        let fixture = TestBed.createComponent(ClsFormComponent)
        self = {
            fixture: fixture,
            component: fixture.componentInstance,
            debugElem: fixture.debugElement,
        }
        self.fixture.detectChanges()
    }

    function resetTypeInput() {
        let selectInput = self.debugElem.query(By.css('#cls-type sui-select'))
        selectInput.nativeElement.value = Type.TRANS_STR
        selectInput.nativeElement.dispatchEvent(new Event('change'))
    }
    
    function clickOn(element: DebugElement) {
        element.triggerEventHandler('click', null)
        self.fixture.detectChanges()
    }
})


class ClsNetworkServiceStub {
    async get(pk: PrimaryKey): Promise<Cls> {
        let cls = new Cls({
            code: 'test mock',
            type: Type.TRANS_STR,
            name: {
                en: 'english name',
                fi: 'not en name',
            },
            productType: {name: 'test', pk: 1},
        })
        return cls
    }
}


class ProductTypeNetworkServiceStub {
    async get(pk: PrimaryKey): Promise<ProductType> {
        return {name: 'Mock product type', pk: 1}
    }
    //noinspection JSUnusedGlobalSymbols
    async getAll(): Promise<Array<ProductType>> {
        return [{name: 'Mock product type', pk: 1}]
    }
}


class OptionNetworkServiceStub {
    async get(pk: PrimaryKey): Promise<Option> {
        return new Option({
            pk: pk,
            clsPk: 1,
            type: Type.TRANS_STR,
            isSaved: true,
        })
    }
    async save(option: Option): Promise<Option> {
        return {} as any
    }
}


/**
 * The `configureTestingModule` method does not support `entryComponents` in
 * angular 4.0.3, so they must be included through a fake module.
 */
@NgModule({
    imports: [
        BrowserModule, FormsModule, HttpModule, SlimLoadingBarModule.forRoot(),
        SimpleNotificationsModule.forRoot(), SuiModule,
    ],
    declarations: [ClsFormComponent, OptionFormComponent, TransStrComponent, IterateEnumPipe],
    entryComponents: [OptionFormComponent],
    exports: [IterateEnumPipe],
    providers: [
        ClsNetworkService, ClsJsonSerializerService,
        ProductTypeNetworkService, NotifyService, NotificationsService,
        OptionJsonSerializerService, CsrfService,
        ProductTypeJsonSerializerService, CookieService,
        {provide: OptionNetworkService, useValue: new OptionNetworkServiceStub()},
        {provide: ClsNetworkService, useValue: new ClsNetworkServiceStub()},
        {provide: ProductTypeNetworkService, useValue: new ProductTypeNetworkServiceStub()},
    ],
})
class FakeModule { }
