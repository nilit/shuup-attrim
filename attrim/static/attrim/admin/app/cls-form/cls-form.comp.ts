import {Component} from '@angular/core'
import {Type} from 'app/type.enum'
import {OptionFormComponent} from 'app/cls-form/fields/option-form.comp'
import {ComponentFactoryResolver} from '@angular/core'
import {ViewContainerRef} from '@angular/core'
import {ComponentRef, ViewChild} from '@angular/core'
import {TypeEnumDecorator} from 'app/decorators/type.enum.decorator'
import {OnInit, ViewEncapsulation} from '@angular/core'
import {Cls} from 'app/models/cls.model'
import {ClsNetworkService} from 'app/services/network/cls.service'
import {ProductType} from 'app/models/product-type.model'
import {ProductTypeNetworkService} from 'app/services/network/product-type.service'
import {NotifyService} from 'app/services/notify.service'
import {SlimLoadingBarService} from 'ng2-slim-loading-bar'
import {asyncShowLoadingBar} from 'app/decorators/async-show-loading-bar.decorator'
import {asyncWithNotify} from 'app/decorators/async-with-notify.decorator'
import {Loadable} from 'app/decorators/async-show-loading-bar.decorator'
import {Notifiable} from 'app/decorators/async-with-notify.decorator'
import {PrimaryKey} from 'app/globals'
import {Window} from 'app/globals'


declare var window: Window


// TODO refactor: move options out into some kind of `options-form.comp`
@Component({
    selector: 'cls-form',
    templateUrl: 'cls-form.comp.html',
    styleUrls: ['cls-form.comp.css'],
    encapsulation: ViewEncapsulation.None,
})
@TypeEnumDecorator
export class ClsFormComponent implements OnInit, Loadable, Notifiable {
    model = new Cls({code: '', type: Type.INT, productType: {name: '', pk: 0}})
    productTypeList: Array<ProductType>

    optionCompRefSet: Set<ComponentRef<OptionFormComponent>> = new Set()
    
    @ViewChild('options', {read: ViewContainerRef})
    private optionsContainerRef: ViewContainerRef

    constructor(
        public loadingBarService: SlimLoadingBarService,
        public notifyService: NotifyService,
        private networkService: ClsNetworkService,
        private productTypeService: ProductTypeNetworkService,
        private componentFactoryResolver: ComponentFactoryResolver,
    ) { }

    async ngOnInit() {
        await this.initForm()
    }

    protected addOption(args?: {
        optionPk?: PrimaryKey | undefined,
        type?: Type | undefined,
        clsPk?: PrimaryKey | undefined,
    }) {
        let optionCompRef: ComponentRef<OptionFormComponent> = this.createOptionComponent()
        if (args) {
            // noinspection JSIgnoredPromiseFromCall
            optionCompRef.instance.initForm(args)
        } else {
            // noinspection JSIgnoredPromiseFromCall
            optionCompRef.instance.initForm({type: this.model.type, clsPk: this.model.pk})
        }
        this.optionCompRefSet.add(optionCompRef)
    }

    /**
     * Don't use the loading bar here because protractor [5.1|5.2] hangs on it with a
     * probability of ~15%.
     */
    // @asyncShowLoadingBar
    @asyncWithNotify({onSuccessMsg: 'The saving complete', onErrorMsg: 'The saving failed'})
    protected async save() {
        let clsSaved = await this.saveCls()
        await this.saveOptions(clsSaved.pk as PrimaryKey)
    }

    @asyncShowLoadingBar
    @asyncWithNotify({onErrorMsg: 'Creation failed'})
    protected async create() {
        this.fillInModelWithDefaultValues(this.model)
        let clsCreated = await this.networkService.create(this.model)
        await this.saveOptions(clsCreated.pk as PrimaryKey)
        location.assign(`/sa/attrim/${clsCreated.pk}/`)
    }
    
    protected selectTypeOptionFormatter(optionValue: Type): string {
        switch (optionValue) {
            case Type.INT:
                return 'integer'
            case Type.DECIMAL:
                return 'decimal'
            case Type.STR:
                return 'string'
            case Type.TRANS_STR:
                return 'translated string'
            default:
                throw new Error('The type is not supported.')
        }
    }

    protected selectProductTypeOptionFormatter(optionValue: ProductType): string {
        return optionValue.name
    }

    private async saveCls(): Promise<Cls> {
        let clsSaved: Cls = await this.networkService.save(this.model)
        this.model = clsSaved
        let productType = this.productTypeList.find(type => type.pk === clsSaved.productType.pk) as ProductType
        this.model.productType = productType
        return clsSaved
    }

    private async saveOptions(clsPk: PrimaryKey) {
        let optionSavePromises: Array<Promise<any>> = []
        for (let optionCompRef of this.optionCompRefSet) {
            optionCompRef.instance.model.clsPk = clsPk
            let savePromise = optionCompRef.instance.save()
            optionSavePromises.push(savePromise)
        }
        await Promise.all(optionSavePromises)
    }

    @asyncShowLoadingBar
    private async initForm() {
        await this.initProductTypeList()
        if (window.DJANGO.isEditForm) {
            await this.loadFormDataFromServer()
        } else {
            this.loadFormDataDefault()
        }
    }

    @asyncWithNotify({onErrorMsg: 'Network error during product types retrieving'})
    private async initProductTypeList() {
        this.productTypeList = await this.productTypeService.getAll()
    }

    private async loadFormDataFromServer() {
        let clsPkToEdit = window.DJANGO.clsPrimaryKey as PrimaryKey
        let clsToEdit = await this.networkService.get(clsPkToEdit)

        this.model = clsToEdit
        let productType = this.productTypeList.find(type => type.pk === clsToEdit.productType.pk) as ProductType
        this.model.productType = productType
        
        for (let optionPk of clsToEdit.optionsPk) {
            this.addOption({optionPk: optionPk})
        }
    }

    private loadFormDataDefault() {
        let clsDefault = new Cls({
            code: '',
            type: Type.INT,
            productType: this.productTypeList[0],
        })
        this.model = clsDefault
    }

    private createOptionComponent(): ComponentRef<OptionFormComponent> {
        let factory = this.componentFactoryResolver
            .resolveComponentFactory(OptionFormComponent)
        return this.optionsContainerRef.createComponent(factory)
    }
    
    private fillInModelWithDefaultValues(model: Cls) {
        let langDefault = window.DJANGO.defaultLang
        let isNameFilled: boolean = (model.name[langDefault] === '') ||
            (model.name[langDefault] === undefined)
        if (isNameFilled) {
            model.name[langDefault] = model.code
        }
    }
}
