export enum Type {
    INT = 1,
    DECIMAL = 3,
    TRANS_STR = 20,
    STR = 21,
}
