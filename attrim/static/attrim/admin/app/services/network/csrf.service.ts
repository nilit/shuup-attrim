import {Injectable} from '@angular/core'
import {CookieService} from 'app/services/network/cookie.service'
import {HttpHeaders} from '@angular/common/http'


@Injectable()
export class CsrfService {
    constructor(
        private cookieService: CookieService,
    ) { }

    getHttpOptions(): {headers: HttpHeaders} {
        let headers = new HttpHeaders({
            'Content-Type': 'application/json',
            'X-CSRFToken': this.cookieService.getByName('csrftoken')!!,
        })
        let options = {headers: headers}
        return options
    }
}
