import {Injectable} from '@angular/core'
import {ProductType} from 'app/models/product-type.model'
import {ProductTypeJsonSerializerService} from 'app/services/network/serializers/product-type-json.service'
import {ProductTypeJson} from 'app/services/network/serializers/product-type-json.service'
import {PrimaryKey} from 'app/globals'
import {HttpClient} from '@angular/common/http'


@Injectable()
export class ProductTypeNetworkService {
    constructor(
        private http: HttpClient,
        private jsonSerializer: ProductTypeJsonSerializerService,
    ) { }

    async get(pk: PrimaryKey): Promise<ProductType> {
        let productTypeJson = await this.http
            .get<ProductTypeJson>(`/api/attrim/product-types/${pk}/`)
            .first()
            .toPromise()
        return this.jsonSerializer.deserialize(productTypeJson)
    }

    async getAll(): Promise<Array<ProductType>> {
        let typeJsonArray = await this.http
            .get<Array<ProductTypeJson>>('/api/attrim/product-types/')
            .first()
            .toPromise()
        return this.jsonSerializer.deserializeArray(typeJsonArray)
    }
}
