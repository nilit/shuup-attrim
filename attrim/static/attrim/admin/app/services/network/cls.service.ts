import {Injectable} from '@angular/core'
import {Cls} from 'app/models/cls.model'
import {ClsJsonSerializerService} from 'app/services/network/serializers/cls-json.service'
import {CsrfService} from 'app/services/network/csrf.service'
import {PrimaryKey} from 'app/globals'
import {HttpClient} from '@angular/common/http'
import {ClsJson} from 'app/services/network/serializers/cls-json.service'


@Injectable()
export class ClsNetworkService {
    constructor(
        private jsonSerializer: ClsJsonSerializerService,
        private csrfService: CsrfService,
        private http: HttpClient,
    ) { }

    async get(pk: PrimaryKey): Promise<Cls> {
        let clsJson = await this.http.get<ClsJson>(`/api/attrim/classes/${pk}/`)
            .first()
            .toPromise()
        let cls = await this.jsonSerializer.deserialize(clsJson)
        return cls
    }

    async save(cls: Cls): Promise<Cls> {
        let clsJson = this.jsonSerializer.serialize(cls)
        let clsSavedJson = await this.http.patch<ClsJson>(
            `/api/attrim/classes/${cls.pk}/`,
            clsJson,
            this.csrfService.getHttpOptions(),
        )
            .first()
            .toPromise()
        let clsSaved = await this.jsonSerializer.deserialize(clsSavedJson)
        return clsSaved
    }

    async create(cls: Cls): Promise<Cls> {
        let clsJson = this.jsonSerializer.serialize(cls)
        let clsJsonCreated = await this.http.post<ClsJson>(
            `/api/attrim/classes/`,
            clsJson,
            this.csrfService.getHttpOptions(),
        )
            .first()
            .toPromise()
        let clsCreated = await this.jsonSerializer.deserialize(clsJsonCreated)
        return clsCreated
    }
}
