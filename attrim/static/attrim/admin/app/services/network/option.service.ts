import {Injectable} from '@angular/core'
import {OptionJsonSerializerService} from 'app/services/network/serializers/option-json.service'
import {Option} from 'app/models/option.model'
import {CsrfService} from 'app/services/network/csrf.service'
import {PrimaryKey} from 'app/globals'
import {HttpClient} from '@angular/common/http'
import {OptionJson} from 'app/services/network/serializers/option-json.service'


@Injectable()
export class OptionNetworkService {
    constructor(
        private jsonSerializer: OptionJsonSerializerService,
        private csrfService: CsrfService,
        private http: HttpClient,
    ) { }

    async get(pk: PrimaryKey): Promise<Option> {
        let optionJson = await this.http.get<OptionJson>(`/api/attrim/options/${pk}/`)
            .first()
            .toPromise()
        let option = await this.jsonSerializer.deserialize(optionJson)
        return option
    }

    async save(option: Option): Promise<Option> {
        let optionJson = this.jsonSerializer.serialize(option)
        let optionSavedJson = await this.http.patch<OptionJson>(
            `/api/attrim/options/${option.pk}/`,
            optionJson, this.csrfService.getHttpOptions(),
        )
            .first()
            .toPromise()
        let optionSaved = await this.jsonSerializer.deserialize(optionSavedJson)
        return optionSaved
    }

    async create(option: Option): Promise<Option> {
        let optionJson = this.jsonSerializer.serialize(option)
        let optionCreatedJson = await this.http.post<OptionJson>(
            `/api/attrim/options/`,
            optionJson, this.csrfService.getHttpOptions(),
        )
            .first()
            .toPromise()
        let optionCreated = await this.jsonSerializer.deserialize(optionCreatedJson)
        return optionCreated
    }

    async delete(option: Option) {
        await this.http.delete(
            `/api/attrim/options/${option.pk}/`,
            this.csrfService.getHttpOptions(),
        )
            .first()
            .toPromise()
    }
}
