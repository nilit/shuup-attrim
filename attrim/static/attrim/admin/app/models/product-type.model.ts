export class ProductType {
    constructor(
        public pk: number,
        public name: string,
    ) { }
}
