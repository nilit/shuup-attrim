export interface TransStr {
    [langCode: string]: string
}
