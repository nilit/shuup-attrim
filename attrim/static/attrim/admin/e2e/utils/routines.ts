import {$} from 'protractor'
import {browser} from 'protractor'
// noinspection TypeScriptPreferShortImport
import {Type} from '../../app/type.enum'
import {$$} from 'protractor'
import {element} from 'protractor'
import {by} from 'protractor'
import {WebElement} from 'selenium-webdriver'


export async function saveCls() {
    await $('#cls-save-button').click()
    await browser.waitForAngular()
}

export async function saveNewCls() {
    await browser.waitForAngularEnabled(false)
    
    await $('#cls-create-button').click()
    
    let isUrlChangedToEditForm = async (): Promise<boolean> => {
        let urlCurrent: string = await browser.getCurrentUrl()
        let isFormEditPageUrl = urlCurrent.match('/sa/attrim/[0-9]+/') !== null
        return isFormEditPageUrl
    }
    await browser.wait(isUrlChangedToEditForm)
    
    let isAngularFormLoaded = async () => await $('cls-form').isPresent() === true
    await browser.wait(isAngularFormLoaded)
    
    await browser.waitForAngularEnabled(true)
}

export async function setClsName(name: string) {
    let nameInputFinder = $('#cls-name trans-str input[data-lang-code="en"]')
    await nameInputFinder.clear()
    await nameInputFinder.sendKeys(name)
}

export async function setClsCode(code: string) {
    let codeInputFinder: WebElement = $('#cls-code input')
    await codeInputFinder.clear()
    await codeInputFinder.sendKeys(code)
}

export async function selectClsType(type: Type) {
    let typeSelectInput: WebElement = $('#cls-type sui-select')
    await typeSelectInput.click()
    
    let typeOption: WebElement = await findClsTypeOption(type)
    await typeOption.click()
}

export async function selectClsProductType(productTypeName: string) {
    await $(`#cls-product-type sui-select`).click()
    await element(by.cssContainingText('sui-select-option', productTypeName)).click()
}

export async function addOption(args: {
    type: Type,
    value: number | string,
    order?: number
}) {
    await $('#add-option').click()
    let option = $$('option-form').last()
    switch (args.type) {
        case Type.STR:
            await option.$(`.option-value-str-input`).clear()
            await option.$(`.option-value-str-input`).sendKeys(args.value)
            break
        case Type.INT:
            await option.$(`.option-value-int-input`).clear()
            await option.$(`.option-value-int-input`).sendKeys(args.value)
            break
        case Type.TRANS_STR:
            await option.$(`trans-str input[data-lang-code="en"]`).clear()
            await option.$(`trans-str input[data-lang-code="en"]`).sendKeys(args.value)
            break
        default:
            throw new Error(`The type is not supported`)
    }
    await option.$(`.option-order-input`).sendKeys(args.order ? args.order : '')
}

async function findClsTypeOption(type: Type): Promise<WebElement> {
    let typeOptionText: string
    switch (type) {
        case Type.INT:
            typeOptionText = 'integer'
            break
        case Type.DECIMAL:
            typeOptionText =  'decimal'
            break
        case Type.STR:
            typeOptionText =  'string'
            break
        case Type.TRANS_STR:
            typeOptionText = 'translated string'
            break
        default:
            throw new Error()
    }
    let typeOptionElem: WebElement
    await $$(`#cls-type sui-select-option`).each(async elementFinder => {
        let text = await elementFinder!!.getText()
        if (text === typeOptionText) {
            typeOptionElem = await elementFinder!!.getWebElement()
        }
    })
    // noinspection JSUnusedAssignment
    return typeOptionElem!!
}
