import {$} from 'protractor'
import {$$} from 'protractor'

import {loadClsEditForm} from './utils/navigation'
import {init} from './utils/init'
import {saveCls} from './utils/routines'
import {addOption} from './utils/routines'
// noinspection TypeScriptPreferShortImport
import {Type} from '../app/type.enum'


describe('option form', () => {
    beforeAll(async done => {
        await init()
        done()
    })

    beforeEach(async done => {
        await loadClsEditForm()
        done()
    })

    it('inits the options', async done => {
        let optionValueInputEn = $$('option-form trans-str input[data-lang-code="en"]').first()
        let optionValueInputEnValue = await optionValueInputEn.getAttribute('value')
        expect(optionValueInputEnValue).toBe('english')
        done()
    })

    it('edits the option value (trans str)', async done => {
        let optionFiLangButtonSelector = `option-form trans-str .button[data-lang-code="fi"]`
        await $$(optionFiLangButtonSelector).first().click()
        
        let optionFiLangInputSelector = `option-form trans-str input[data-lang-code="fi"]`
        let optionFiLangInput = $(optionFiLangInputSelector)
        let optionValueInputFiValueNew = 'new fi str'
        await optionFiLangInput.clear()
        await optionFiLangInput.sendKeys(optionValueInputFiValueNew)

        await saveCls()

        await loadClsEditForm()

        await $$(optionFiLangButtonSelector).first().click()
        let optionValueInputFiValue = await $(optionFiLangInputSelector).getAttribute('value')
        expect(optionValueInputFiValue).toBe(optionValueInputFiValueNew)

        done()
    })

    it('can add and delete an option', async done => {
        let optionAddedValue = 'option added value'
        await addOption({type: Type.TRANS_STR, value: optionAddedValue})
        await saveCls()

        // check that the option was created
        await loadClsEditForm()
        let optionSelector = `option-form trans-str input[data-lang-code="en"]`
        let optionAddedFinder = $$(optionSelector).last()
        let optionAddedValueActual = await optionAddedFinder.getAttribute('value')
        expect(optionAddedValueActual).toBe(optionAddedValue)

        // remove the option
        let optionAddedRemoveInput = $$('option-form .option-remove').last()
        await optionAddedRemoveInput.click()
        await saveCls()

        // check that the option was removed
        await loadClsEditForm()
        let optionLastInputValue = await $$(optionSelector).last().getAttribute('value')
        expect(optionLastInputValue).not.toBe(optionAddedValue)

        done()
    })
})
