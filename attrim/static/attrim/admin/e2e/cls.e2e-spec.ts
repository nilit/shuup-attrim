import {promise as wdpromise} from 'selenium-webdriver'
import {WebElement} from 'selenium-webdriver'
import {$} from 'protractor'
import {$$} from 'protractor'
import {ElementFinder} from 'protractor'

// noinspection TypeScriptPreferShortImport
import {Type} from '../app/type.enum'
import {init} from './utils/init'
import {loadClsNewForm} from './utils/navigation'
import {saveCls} from './utils/routines'
import {loadClsEditForm} from './utils/navigation'
import {saveNewCls} from './utils/routines'
import {setClsName} from './utils/routines'
import {selectClsType} from './utils/routines'
import {addOption} from './utils/routines'
import {setClsCode} from './utils/routines'
import {selectClsProductType} from './utils/routines'


describe('cls form', () => {
    beforeAll(async done => {
        await init()
        done()
    })

    it('edits the product type', async done => {
        await loadClsEditForm()

        await selectClsProductType('Digital Product')

        await saveCls()

        await loadClsEditForm()

        let selectedProductType = await $(`#cls-product-type sui-select .text`).getText()
        expect(selectedProductType).toBe('Digital Product')

        done()
    })

    it('correctly reloads the product type from server after an edit', async done => {
        await loadClsEditForm()

        let productTypeName = 'Standard Product'
        await selectClsProductType(productTypeName)

        await saveCls()

        let selectedProductTypeName = await $(`#cls-product-type sui-select .text`).getText()
        expect(selectedProductTypeName).toBe(productTypeName)

        done()
    })

    it('edits the default class name', async done => {
        await loadClsEditForm()

        let clsNameInputEn = $('#cls-name trans-str input[data-lang-code="en"]')
        let clsNameEnOld = await clsNameInputEn.getAttribute('value')
        let clsNameEnNew = 'new english name'
        await setClsName(clsNameEnNew)
        await saveCls()

        await loadClsEditForm()
        let clsNameInputEnUpdated = $('#cls-name trans-str input[data-lang-code="en"]')
        expect(await clsNameInputEnUpdated.getAttribute('value')).toBe(clsNameEnNew)

        // rollback to the changes for the other tests
        await setClsName(clsNameEnOld)
        await saveCls()

        done()
    })

    it('creates an INT class with no options', async done => {
        await loadClsNewForm()

        let clsCode = 'cls_code'

        await setClsCode(clsCode)
        await setClsName('name')

        await saveNewCls()

        let clsCodeInputValue = await $('#cls-code input').getAttribute('value')
        expect(clsCodeInputValue).toBe(clsCode)

        done()
    })

    it('creates a STR class with options', async done => {
        await loadClsNewForm()

        await setClsCode('cls_str_code')
        await setClsName('name')

        await selectClsType(Type.STR)

        let optionValue1 = 'value1'
        let optionValue2 = 'value2'
        await addOption({type: Type.STR, value: optionValue1, order: 1})
        await addOption({type: Type.STR, value: optionValue2, order: 2})

        await saveNewCls()

        await expectOptionValuePresent(Type.STR, optionValue1)
        await expectOptionValuePresent(Type.STR, optionValue2)

        done()
    })

    it('creates an INT class with options', async done => {
        await loadClsNewForm()

        await setClsCode('cls_int_code')
        await setClsName('name')

        await selectClsType(Type.INT)

        let optionValue1 = 1
        let optionValue2 = 2
        await addOption({type: Type.INT, value: optionValue1, order: 1})
        await addOption({type: Type.INT, value: optionValue2, order: 2})

        await saveNewCls()

        await expectOptionValuePresent(Type.INT, optionValue1)
        await expectOptionValuePresent(Type.INT, optionValue2)

        done()
    })

    async function expectOptionValuePresent(type: Type, value: string | number) {
        let filterOptionInputByValue = (input: WebElement): wdpromise.Promise<boolean> => {
            return input.getAttribute('value').then(inputVal => {
                return inputVal === String(value)
            })
        }
        let optionInputFinder: ElementFinder
        switch (type) {
            case Type.STR:
                optionInputFinder = $$('option-form .option-value-str-input')
                    .filter(filterOptionInputByValue)
                    .first()
                break
            case Type.INT:
                optionInputFinder = $$('option-form .option-value-int-input')
                    .filter(filterOptionInputByValue)
                    .first()
                break
            default:
                throw new Error(`The type is not supported`)
        }
        let isOptionInputFound = await optionInputFinder.isPresent()
        expect(isOptionInputFound).toBe(true)
    }
})
