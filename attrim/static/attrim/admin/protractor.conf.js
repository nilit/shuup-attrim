exports.config = {
    framework: 'jasmine',
    capabilities: {
        browserName: 'chrome',
        chromeOptions: {
            'args': ['--no-sandbox'],
        },
    },
    specs: [
        './e2e/cls.e2e-spec.js',
        './e2e/options.e2e-spec.js',
    ],
    seleniumServerJar: './node_modules/selenium-server-standalone-jar/jar/selenium-server-standalone-3.7.1.jar',
    SELENIUM_PROMISE_MANAGER: false,
}
