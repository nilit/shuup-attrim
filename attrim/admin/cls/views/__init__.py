from .list import ClassListView
from .edit import ClassEditView
from .delete import ClassDeleteView


__all__ = [
    'ClassListView',
    'ClassEditView',
    'ClassDeleteView',
]
