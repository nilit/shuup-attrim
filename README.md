# Shuup Attrim

This addons allows to create multi-value attributes for Shuup.

## Screenshots

Creation of the class `color` and assignment of a `color` attribute to a product:

<a href="https://gitlab.com/nilit/shuup-attrim/raw/master/docs/creation-form.png">
    <img src="https://gitlab.com/nilit/shuup-attrim/raw/master/docs/creation-form.png" width="377px">
</a>
<a href="https://gitlab.com/nilit/shuup-attrim/raw/master/docs/assignment-form.png">
    <img src="https://gitlab.com/nilit/shuup-attrim/raw/master/docs/assignment-form.png" width="377px">
</a>

## Installation

Install using `pip`:
```
pip install shuup-attrim
```

Add `attrim` to your `INSTALLED_APPS` setting:
```
INSTALLED_APPS = (
    # [...]
    'attrim',
)
```

Apply the migrations:
```
python manage.py migrate
```

Collect the static files:
```
python manage.py collectstatic
```
